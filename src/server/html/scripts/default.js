var app = angular.module('assessment', []);
app.controller('defaultCtrl', function($scope, $http) {
    $scope.allmessages= [];
    $scope.msg = "";

    $scope.sendmessage=function(){
        $http.post('http://localhost:8084/api/message', {msg: $scope.msg } ).
            then(function(response) {
                $scope.allmessages.push($scope.msg);
            });
    };

    $scope.refresh=function(){

        $http.get('http://localhost:8084/api/message').
            then(function(response) {
                $scope.allmessages = response.data;
            });
    }

    //Refresh on reload
    $http.get('http://localhost:8084/api/message').
        then(function(response) {
            $scope.allmessages = response.data;
        });

});