var express = require('express');
var app = express();
const bodyParser = require('body-parser')

var allmessages = [ 'READY' ];

app.use(bodyParser.json())
app.use(express.static('html'))

app.get('/api/message', function(req, res) {
  res.json(allmessages);
});

app.post('/api/message', function(req, res) {
    allmessages.push(req.body.msg);
    res.sendStatus(200);
});

app.listen(process.env.PORT || 8084);